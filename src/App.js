import React from 'react';
import './App.css';

class App extends React.Component {

  constructor(props) {

    super(props);

    this.handleChange = this.handleChange.bind(this);

    this.state = {
      word: '',
    }

  }

  handleChange( e ) {
    this.numsToWords(e.target.value);
  }

  numsToWords( num ) {

    const stringifiedNumber = num.toString().replace(/\s/g,''),
          serializingKeyword = stringifiedNumber.length < 3 ? '' : 'and';

    const singles = [
      '', 
      'one', 
      'two', 
      'three', 
      'four', 
      'five', 
      'six', 
      'seven', 
      'eight', 
      'nine', 
      'ten', 
      'eleven', 
      'twelve', 
      'thirteen', 
      'fourteen', 
      'fifteen', 
      'sixteen', 
      'seventeen', 
      'eighteen', 
      'nineteen'
    ];
    
    const tens = [
      '', 
      '', 
      'twenty', 
      'thirty', 
      'forty', 
      'fifty', 
      'sixty', 
      'seventy', 
      'eighty', 
      'ninety'
    ];
    
    const descriptors = [
      '', 
      'thousand', 
      'million', 
      'billion', 
      'trillion', 
      'quadrillion',
      'to infinity, and beyond!'
    ];

    let numGroups,
        numGroup, 
        currentNumGroup, 
        singleNumbers,
        beginning, 
        end,
        numbersChain,
        finalWord;

    
    if ( isNaN(stringifiedNumber) ) { // Only run conversion for numbers

      this.setState({
        word: "This ain't a number!"
      })

      return false;

    }

    if ( stringifiedNumber === '' ) {

      this.setState({
        word: ''
      });

      return false;
    }


    if ( parseInt(stringifiedNumber) === 0 ) {

      this.setState({
        word: 'Zero'
      })

      return false;
    }
    
    beginning = stringifiedNumber.length;

    numGroups = [];

    while ( beginning > 0 ) {
        end = beginning;
        numGroups.push( stringifiedNumber.slice( ( beginning = Math.max(0, beginning - 3) ), end ) );
    }

    numbersChain = [];

    for ( let i = 0 ; i < numGroups.length ; i++ ) {

      numGroup = parseInt( numGroups[i] );

      if ( numGroup ) {

        singleNumbers = numGroups[i].split('').reverse().map( parseFloat );

        /* 
          Dictionary:

          singleNumbers[0] === singles
          singleNumbers[1] === tens
          singleNumbers[2] === hundreds
          
        */

        singleNumbers[0] = singleNumbers[1] === 1 ? ( singleNumbers[0] + 10 ) : singleNumbers[0];

        
        // Apply descriptor if current number group is !0 and item exists in collection
        if ( ( currentNumGroup = descriptors[i] ) ) {
            numbersChain.push( currentNumGroup );
        }

        

        if (( currentNumGroup = singles[ singleNumbers[0] ] )) { // Append singles
            numbersChain.push( currentNumGroup );
        }

        

        if (( currentNumGroup = tens[ singleNumbers[1] ] )) { // Append tens
            numbersChain.push( currentNumGroup );
        }

        

        if ( singleNumbers[0] || singleNumbers[1] ) { // Append serializing keyword for singles / tens

            if ( singleNumbers[2] || (!i && numGroups.length) ) { // Number group has a hundreds
                numbersChain.push(serializingKeyword);
            }

        }



        if (( currentNumGroup = singles[ singleNumbers[2] ] )) {
            numbersChain.push( currentNumGroup + ' hundred' );
        }

      }

    }

    
    numbersChain.reverse();
    numbersChain = numbersChain.filter( (n) => {
      return n !== '';
    });

    numbersChain[0] = numbersChain[0].charAt(0).toUpperCase() + numbersChain[0].slice(1);

    finalWord = numbersChain.join(' ');

    this.setState({
      word: finalWord
    })

}

  render() {

    return (
      <div className="App">

        <InputField onChange={ (e) => this.handleChange(e) } />
        <div className="title">Or in other words...</div>
        <h1>{ this.state.word }</h1>

      </div>
    );
  }
}

class InputField extends React.Component {

  constructor(props) {
    super(props);
    this.onChange = this.onChange.bind(this);
  }

  onChange(e) {
    this.props.onChange(e);
  }

  render() {
    return ( 
      <input 
        type="text"
        onChange={ (e) => this.props.onChange(e) }
        placeholder="Enter number here..." 
      /> )
  }
}

export default App;
